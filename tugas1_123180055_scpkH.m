"Nomor 1"
matrik1horizontal = [10 20 30 40]
matrik1vertikal = [-5;-15;-40]
matriks1 = [1 3 5 0;3 1 3 5;5 3 1 3;0 5 3 1]

"Nomor 2"
matriks2 = [1 2 3;4 5 6;7 8 -9]
matriks2hasil = [2;-5.5;-49]
xyz2 = inv(matriks2)*matriks2hasil

"Nomor 3"
M3 = [10 20;5 8]
N3 = [-1 1;1 -1]
a3 = M3+N3
b3 = M3-N3
c3 = N3+9
d3a = M3*N3
d3b = N3*M3

"Nomor 4"
a4 = [0 5 5]
b4 = [1 1 1]
a4dotb4 = dot(a4,b4)
a4crossb4 = cross(a4,b4)
b4crossa4 = cross(b4,a4)

"Nomor 5"
a5 = [4 8;2 4]
b5 = [1 1;1 -1]
c5 = [a5 b5]
w5 = [b5 b5;b5 -b5]

"Nomor 6"
identitas6 = eye(3)
nol6 = zeros(3)
one6 = ones(3)

"Nomor 7"
mean7 = 1
variansi7 = 0.2
gauss7 = sqrt(variansi7)*randn(1,100)+mean7

"Nomor 8"
a8 = -10:10
a8lin = linspace(-10,10,21)
b8 = -7.5:0.5:0
b8lin = linspace(-7.5,0,16)
c8 = 1:3:100
c8lin = linspace(1,100,34)
d8log = logspace(-3,6,10)

"Nomor 9"
m9 = [1 5 10 15 20;1 2 4 8 16;-3 0 3 6 9;32 16 8 4 2;5 -5 5 -5 5]
m9baris1 = m9(1,:)
m9kolom3 = m9(:,3)
m9baris35kolom24 = m9(3:5,2:4)
m9diag = diag(m9)

"Nomor 10"
m10 = m9(1:4,:)
m10 = m9(:,1:4)
m10a = fliplr(m10)
m10b = flipud(m10)
m10c = reshape(m10,10,2)
m10d = reshape(m10,4,5)
